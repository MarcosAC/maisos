﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MaisOS.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListaServicosView : ContentPage
	{
		public ListaServicosView ()
		{
			InitializeComponent ();
		}

        private void AdcionarServico_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ServicosView());
        }
    }
}