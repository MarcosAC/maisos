﻿using MaisOS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MaisOS.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MasterPageView : MasterDetailPage
    {
        private List<MasterPageItem> MenuItemLista { get; set; }

        public MasterPageView ()
		{
			InitializeComponent ();

            MenuItemLista = new List<MasterPageItem>
            {
                new MasterPageItem() { Title = "Principal", Icon = "principal.png", TargetType = typeof(PrincipalView) },
                new MasterPageItem() { Title = "Ordem de Serviço", Icon = "ordemservico.png", TargetType = typeof(ListaOrdemDeServicoView) },
                new MasterPageItem() { Title = "Configurações", Icon = "configuracoes.png", TargetType = typeof(ConfiguracoesView) }
            };

            MenuItem.ItemsSource = MenuItemLista;

            Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(PrincipalView)));
        }

        private void OnMenuItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;

            var item = (MasterPageItem)e.SelectedItem;
            Type page = item.TargetType;

            Detail = new NavigationPage((Page)Activator.CreateInstance(page));
            IsPresented = false;
            MenuItem.SelectedItem = null;
        }
    }
}