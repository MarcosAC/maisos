﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MaisOS.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class OrdemDeServicoView : ContentPage
	{
		public OrdemDeServicoView ()
		{
			InitializeComponent ();

            PckStatus.SelectedIndex = 0;
        }

        private void OnDataInicialSelect(object sender, DateChangedEventArgs e)
        {

        }

        private void OnDataFinalSelect(object sender, DateChangedEventArgs e)
        {

        }
    }
}