﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MaisOS.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListaProdutosView : ContentPage
	{
		public ListaProdutosView ()
		{
			InitializeComponent ();
		}

        private void AdcionarProduto_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ProdutosView());
        }
    }
}